package crackingbook.chapter1;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Solution2Test {

    private Solution2 sol = new Solution2();

    //negative tests
    @Test
    public void almostIdenticalStringAreStillNotPermutationOfEachOther() {
        assertFalse(sol.oneIsPermutationOfAnother("123456789", "12345678"));
        assertFalse(sol.oneIsPermutationOfAnother("123123", " 123123"));
        assertFalse(sol.oneIsPermutationOfAnother("123123", "123123 "));
    }

    @Test
    public void notEvenLooksTheSameNonPermutations() {
        assertFalse(sol.oneIsPermutationOfAnother("sdfgdgfshsdfghj", "12345678"));
        assertFalse(sol.oneIsPermutationOfAnother("uiegrhhuiop345tyhuiphuip", " njuioaegruoaegtrhhuioaegrhuio"));
        final String actualString = "uiegrhhuiop345tyhuiphuip";
        // one p deleted
        final String almostPermutation = "op3iegrtyuih4uhhu5huiip";
        assertFalse(sol.oneIsPermutationOfAnother(actualString, almostPermutation));
    }


    // exception tests

    @Test(expected = IllegalArgumentException.class)
    public void nullStringShouldThrowIllegalArgException1() {
        sol.oneIsPermutationOfAnother(null, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullStringShouldThrowIllegalArgException2() {
        sol.oneIsPermutationOfAnother("", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullStringShouldThrowIllegalArgException3() {
        sol.oneIsPermutationOfAnother(null, null);
    }

    // positive tests

    @Test
    public void singleLetterAlwaysTrue() {
        assertTrue(sol.oneIsPermutationOfAnother("a", "a"));
        assertTrue(sol.oneIsPermutationOfAnother(" ", " "));
        assertTrue(sol.oneIsPermutationOfAnother("^", "^"));
    }

    @Test
    public void emptyStringIsPermutationOfItself() {
        assertTrue(sol.oneIsPermutationOfAnother("", ""));
    }

    @Test
    public void actualPermutation() {
        assertTrue(sol.oneIsPermutationOfAnother("hello", "olleh"));
        assertTrue(sol.oneIsPermutationOfAnother("hello", "ohell"));
        assertTrue(sol.oneIsPermutationOfAnother("hell%24 ^&##o", " o#^%h2&el4l#"));
    }

    @Test
    public void actualPermutationWithStringDuplication() {
        assertTrue(sol.oneIsPermutationOfAnother("ppppppppp", "ppppppppp"));
        assertTrue(sol.oneIsPermutationOfAnother("&&&&d&sf&&&&a&&", "&&s&&af&&&&d&&&"));
    }

    @Test
    public void twoSameStrings() {
        assertTrue(sol.oneIsPermutationOfAnother("asdfghjkl", "asdfghjkl"));
    }

    @Test
    public void logLinesPermutation() {
        final String str = generateRandString();
        assertTrue(sol.oneIsPermutationOfAnother(str, shuffleString(str)));
    }

    // quite random
    private static String generateRandString() {
        return "123iuo5rhq39o7245t6hy249p576h83490p567hyow3456;790358-76u934586hi24yu5g6u23io4656g348o5t7gh3ib 3il7v y49576tughyi 425il6ygb 23o4ui56lyg ui24l5yg6vbui24l 5kyvg6uil245yg6 ";
    }

    // Implementing Fisher–Yates shuffle that i've just copypasted
    private static String shuffleString(String s)
    {
        char[] ar = s.toCharArray();
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            char a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return new String(ar);
    }




}