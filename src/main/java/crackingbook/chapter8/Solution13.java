package crackingbook.chapter8;

import java.util.Set;

/**
 * Stack of Boxes: You have a stack of n boxes, with widths wi
 , heights hi
 , and depths di
 . The boxes
 cannot be rotated and can only be stacked on top of one another if each box in the stack is strictly
 larger than the box above it in width, height, and depth. Implement a method to compute the
 height of the tallest possible stack. The height of a stack is the sum of the heights of each box.
 */
public class Solution13 {

    public int maxHeight(Set<Box> boxes) {
        return -1;
    }

}

class Box {
    int height;
    int width;
    int depth;

    public Box(int height, int width, int depth) {
        this.height = height;
        this.width = width;
        this.depth = depth;
    }

    boolean couldBeOnTop(Box box) {
        return height > box.height && width > box.width && depth > box.depth;
    }
}
