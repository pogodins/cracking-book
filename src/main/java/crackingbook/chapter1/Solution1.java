package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * implement an algorithm to determine if a string has all unique characters, you cannot
 * use additional data structures. o(1) space
 */
public class Solution1 {
    public boolean hasAllUniqueChars(String str) {
        char[] string = str.toCharArray();

        // TODO: implement for array of chars
        throw new NoImplException();
    }
}
