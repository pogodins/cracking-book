package crackingbook.chapter8;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * Created by Root on 4/18/2017.
 */
public class Solution14Test {

    Solution14 sol = new Solution14();

    @Test
    public void successfulSol1() {
        assertThat(sol.countEval("1^0|0|1", false), CoreMatchers.is(2));
    }

    @Test
    public void successfulSol2() {
        assertThat(sol.countEval("0&0&0&1^1|0", true), CoreMatchers.is(10));
    }

}