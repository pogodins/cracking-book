package crackingbook.chapter8;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**

 */
public class Solution6Test {

    @Test
    public void successTest() {
        final int number = 25;
        Solution6 sol = new Solution6(number);

        Solution6.Tower tow = sol.getAnotherFullFiledTower();

        assertThat(tow.size(), CoreMatchers.equalTo(number));
        for (int i = 1; i <= number; i++) {
            assertThat(tow.pop(), CoreMatchers.equalTo(i));
        }
    }

}