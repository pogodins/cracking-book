package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * There are three types of edit : insert a char, remove a char, replace a char.
 * Given a two strings : method checking if they a one or zero edits away from each other
 * E.G.
 * pale, ple -> true
 * pales, pale -> true
 * pale, bale -> true
 * pale, bae -> false
 */
public class Solution5 {
    public boolean oneAway(String str1, String str2) {
        throw new NoImplException();
    }
}
