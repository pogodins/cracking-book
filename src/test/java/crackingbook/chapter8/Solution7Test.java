package crackingbook.chapter8;

import com.google.common.collect.Sets;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class Solution7Test {

    @Test
    public void successTest() {
        final String s = "omg";
        Solution7 sol = new Solution7();
        Set<String> expected = Sets.newHashSet("omg", "mgo", "ogm", "mog", "gom", "gmo");

        Set<String> actual = sol.permutations(s);
        for (String exp : expected) {
            assertTrue("\n wait a minute, it's not a correct data : " + actual + " \n we expected" + expected,
                    actual.contains(exp));
        }

    }

}