package crackingbook.chapter1;

import crackingbook.exception.NoImplException;
import crackingbook.util.Solution9Util;

/**
 * Here you have a method isSubstring; it checks if one word is a substring of another;
 * Using this method only once: check if one string is a rotation of another
 *
 * Rotation: cut a string into 2 parts: s1 and s2 and rearrange them;
 * str = s1 + s2 => rotation = s2 + s1
 * E.G. hellobadass -> obadasshell
 */
public class Solution9 {

    public boolean isRotation(String s1, String s2) {
        final Solution9Util s = new Solution9Util();

        //TODO : remove exception; place implementation here
        // TODO : use s.isSubstring
        throw new NoImplException();
    }



}
