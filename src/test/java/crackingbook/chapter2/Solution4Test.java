package crackingbook.chapter2;

import crackingbook.util.LList;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * Created by Root on 1/11/2017.
 */
public class Solution4Test {


    Solution4 sol = new Solution4();

    @Test
    public void positiveTestsStd() {
        LList<Integer> list = LList.generateLList(3,4,6,7,8,3,6,9,5);
        LList<Integer> result = sol.partialSort(list, 5);
        LList<Integer> expected = LList.generateLList(3,4,3,6,7,8,6,9,5);

        assertThat("res:" + result + "  exp:" + expected,result, CoreMatchers.equalTo(expected));
    }

    @Test
    public void positiveTestReversalList() {
        LList<Integer> list = LList.generateLList(9,8,43,77,4,3,5,6,1,2,3,4);
        LList<Integer> result = sol.partialSort(list, 5);
        LList<Integer> expected = LList.generateLList();

        assertThat("res:" + result + "  exp:" + expected,result, CoreMatchers.equalTo(expected));
    }

}