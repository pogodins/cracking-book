# "Cracking the code interview" book solutions #

### How to ###

* Check out master branch
* Branch-out with you own branch
* Create classes following the existing concept: crackingbook.chapter1.Solution1 class for task 1.1 and so on. See already existent code
* Share with your unit tests if you think they are good enough - add them to master branch
    * How to: `git checkout master` `git checkout serg -- src/test/java/crackingbook/chapter1` where serg is your branch

**Don't commit any solutions to master branch**, only the stub classes (having declaration, but no implementation) and tests.