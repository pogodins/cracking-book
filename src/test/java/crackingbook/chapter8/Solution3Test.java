package crackingbook.chapter8;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertThat;

/**

 */
public class Solution3Test {

    private Solution3 sol = new Solution3();

    @Test
    public void firstSuccess() {
        //given
        int[] arr = {-1,0,2,5,8,13};
        int expected = 2;
        int result = sol.findMagicIndex(arr);

        assertThat(result, CoreMatchers.equalTo(expected));
    }

    @Test
    public void noMagicNumbers() {
        //given
        int[] arr = {1,2,34,5,6,7,8};
        int expected = -1;
        int result = sol.findMagicIndex(arr);

        assertThat(result, CoreMatchers.equalTo(expected));
    }

    @Test
    public void magicZero() {
        //given
        int[] arr = {0};
        int expected = 0;
        int result = sol.findMagicIndex(arr);

        assertThat(result, CoreMatchers.equalTo(expected));
    }

    @Test
    public void complexitySurvival() {
        int [] arr = new int[Integer.MAX_VALUE / 16];
        Random rand = new Random();
        System.out.println("started");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(500000);
        }
        System.out.println("started");
        Arrays.sort(arr);
        System.out.println("started");
        long start = System.currentTimeMillis();
        int a = sol.findMagicIndex(arr);
        long end = System.currentTimeMillis();
        System.out.println("" + a + " " + (end - start));
        Assert.assertTrue((end-start) < 25);
    }

}