package crackingbook.chapter8;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static crackingbook.chapter8.Direction.D;
import static crackingbook.chapter8.Direction.R;
import static org.hamcrest.CoreMatchers.equalTo;

/**

 */
public class Solution2Test {

    private Solution2 sol = new Solution2();

    @Test
    public void correctTraversal() {
        final int [][] init = {{1,1,0,1,1,1,1},
                               {0,1,1,1,1,1,0},
                               {0,1,0,1,0,1,1}};

        final List<Direction> expectedDirection = Arrays.asList(R,D,R,R,R,R,D,R);
        final List<Direction> expectedDirectionVisaVersa = Arrays.asList(D,R,D,D,D,D,R,D);
        List<Direction> actualList = sol.findAWay(transfer(init));

        if (expectedDirectionVisaVersa.equals(actualList)) {
            Assert.fail("expected: \n" + expectedDirection + "\n and you have visa - versa naming list \n"
                    + actualList + "\n you can easily fix this bug by replacing R to D ");
        } else {
            Assert.assertThat(actualList, equalTo(expectedDirection));
        }
    }

    private boolean[][] transfer(int [][] a) {
        boolean[][] b = new boolean[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                b[i][j] = a[i][j] == 1;
            }
        }
        return b;
    }
}