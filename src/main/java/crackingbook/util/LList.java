package crackingbook.util;

/**
 * Created by Root on 1/11/2017.
 */
public class LList<T> {

    public LList<T> next;
    public T val;

    public LList(T val) {
        this(val, null);
    }

    public LList(T val,LList<T> next) {
        this.next = next;
        this.val = val;
    }

    public static <T>LList<T> generateLList(T... vals) {
        LList<T> first = new LList<>(vals[0]);
        LList<T> prev = first;
        for (int i = 1; i < vals.length; i++) {
            LList<T> curr = new LList<>(vals[i]);
            prev.next = curr;
            prev = curr;
        }
        return first;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LList<?> lList = (LList<?>) o;

        if (next != null ? !next.equals(lList.next) : lList.next != null) return false;
        return val != null ? val.equals(lList.val) : lList.val == null;
    }

    @Override
    public int hashCode() {
        int result = next != null ? next.hashCode() : 0;
        result = 31 * result + (val != null ? val.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "" + val + "->" + ((next == null)?"}":"" + next);
    }
}
