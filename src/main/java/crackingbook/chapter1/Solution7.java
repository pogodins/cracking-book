package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * Image represented as NxX matrix; each pixel = 4 bytes;
 * Rotate matrix by 90 degrees.
 * Try to do it in place.
 */
public class Solution7 {
    /**
     *
     * @param matrix will be changed during rotation
     * @return false if matrix is not square
     */
    boolean rotate(int [][] matrix) {
        throw new NoImplException();
    }
}
