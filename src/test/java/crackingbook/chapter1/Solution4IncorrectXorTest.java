package crackingbook.chapter1;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Root on 1/6/2017.
 */
public class Solution4IncorrectXorTest {
    Solution4IncorrectXor sol = new Solution4IncorrectXor();

    @Test
    public void positivePolindomTest() {
        // this tests works ok with xor implementation
        assertTrue(sol.isPalindromePermutation("asd fsd a"));
        assertTrue(sol.isPalindromePermutation(" dfghbsdfjgkhv asan 1235kldafjbg dfghbsdfjgkhv asan 1235kldafjbg  191    "));
        assertTrue(sol.isPalindromePermutation("     "));
        assertTrue(sol.isPalindromePermutation("  @#$%^&*&^%$# +_+_--@uplz ][apuzal][ "));

    }

    /*
    this test will fail of xor implementation
     */
    @Ignore
    @Test
    public void negativePolindomTest() {
        assertFalse(sol.isPalindromePermutation("asd fd a"));
        assertFalse(sol.isPalindromePermutation(" dfghbsdfjghv asan 1235kldafjbg dfghbsdfjgkhv asan 1235kldafjbg  191    "));
        assertFalse(sol.isPalindromePermutation("    ds "));
        assertFalse(sol.isPalindromePermutation("  @#$%^&*&^%# +_+_--@uplz ][apuzal][ "));
    }

    /*
    this test will fail of xor implementation
     */
    @Ignore
    @Test
    public void xorImplementationTest() {
        //āခᄀ
        // this test fails in case of xor implementation; which is partially correct only
        assertFalse(sol.isPalindromePermutation("" + '\u0101' + '\u1001' + '\u1100'));
    }

}