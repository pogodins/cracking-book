package crackingbook.chapter1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by Root on 1/6/2017.
 */
public class Solution3Test {

    Solution3 sol = new Solution3();

    @Test
    public void positiveTest() {
        proceedATest("Mr John Smith    ", 13, "Mr%20John%20Smith");
        proceedATest("hello how are you      ", 17, "hello%20how%20are%20you");
        int len = " the tricky string ".length();
        proceedATest(" the tricky string         ", len, "%20the%20tricky%20string%20");
    }

    @Test
    public void emptyStringReturnsItself() {
        proceedATest("", 0, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullCausesException() {
        sol.replaceSpaces(null, 123);
    }

    private void proceedATest(String incoming, int l, String exp) {
        assertThat(sol.replaceSpaces(incoming.toCharArray(), l), equalTo(exp.toCharArray()));
    }

}