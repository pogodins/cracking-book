package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * Write a method to replace all spaces in a string with '%20'. String has additional space in the end to fit all of the characters:
 * True length is the length of valuable string counting from the start
 * E.G.
 * Input: str: "Mr John Smith    ", trueLength: 13
 * Output: "Mr%20John%20Smith"
 *
 * 13 = "Mr John Smith".length()
 */
public class Solution3 {
    public char[] replaceSpaces(char[] str, int trueLen) {
        throw new NoImplException();
    }

}
