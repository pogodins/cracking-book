package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * Palindrome Permutation:
 * Given a string, white a method to check if it is a permutation of palindrome.
 * String can contain any characters
 * E.G.
 * Input: tact coa
 * Output: true (Because: taco cat, atcocta, etc.)
 */
public class Solution4 {
    public boolean isPalindromePermutation(String str) {
        throw new NoImplException();
    }
}
