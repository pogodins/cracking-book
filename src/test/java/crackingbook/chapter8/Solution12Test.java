package crackingbook.chapter8;

import crackingbook.chapter8.Solution12.Board;
import crackingbook.chapter8.Solution12.Figure;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static crackingbook.chapter8.Solution12.Figure.K;
import static crackingbook.chapter8.Solution12.Figure.Q;
import static org.junit.Assert.assertThat;

/**
 * Created by Root on 4/15/2017.
 */
public class Solution12Test {

    @Test
    public void boardDrawCorrect() {
        Board b = new Board(3);
        System.out.println(b.toString());
        String expectations = "***\n___\n___\n___\n***\n";
        assertThat(b.toString(), CoreMatchers.equalTo(expectations));

        Board b2 = new Board(3);
        b2.set(Q, 1, 1);
        b2.set(K, 0, 0);
        b2.set(K, 2, 2);
        b2.set(K, 0, 2);
        b2.set(K, 2, 0);
        System.out.println(b2.toString());
        String expectations2 = "****\nK_K\n_Q_\nK_K\n***\n";
        assertThat(b2.toString(), CoreMatchers.equalTo(expectations2));
    }

    @Test
    public void successTest() {
        Solution12 sol = new Solution12();
        Set<Board> res = sol.arrangeQueens(4);
        String [] expectedBoards = new String[]{"_Q_____QQ_____Q_", "__Q_Q______Q_Q__"};

        assertThat(res, CoreMatchers.equalTo(bunchOfBoards(4, expectedBoards)));

    }

    /**
     * line of queens empty line line of kings empty line will looks like :
     * @param size = 4
     * @param boards = "QQQQ____KKKK____"
     * @return
     */
    private Set<Board> bunchOfBoards(int size, String[] boards) {
        Set<Board> boardSet = new HashSet<>();
        for (String boardAsString : boards) {
            Board board = new Board(size);
            int counter = 0;
            for(int i = 0; i < size; i ++) {
                for(int j = 0; j < size; j++) {
                    Figure curr = Figure.valueOf(boardAsString.charAt(counter) + "");
                    board.set(curr, i , j);
                    counter ++;
                }

            }
            boardSet.add(board);
        }
        return boardSet;

    }

}