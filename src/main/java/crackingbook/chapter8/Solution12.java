package crackingbook.chapter8;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Set;

/**
 * Eight Queens: Write an algorithm to print all ways of arranging eight queens on an 8x8 chess board
 so that none of them share the same row, column, or diagonal. In this case, "diagonal" means all
 diagonals, not just the two that bisect the board.
 */
public class Solution12 {

    public static int NUMBER_OF_QUEENS = 8;

    public Set<Board> arrangeQueens(int n) {
        //TODO impl
        //TODO : n should be equal to NUMBER_OF_QUEENS
        return null;
    }


    public static class Board {
        private final int[][] board;
        private final int magicalSize;
        private final String prefix;

        public Board(int boardSize) {
            this.magicalSize = boardSize;
            this.board = new int[magicalSize][magicalSize];
            prefix = StringUtils.repeat("*", magicalSize) + "\n";
        }

        public Board() {
            this(8);
        }

        public Figure get(int row, int col) {
            validation(row,col);
            return Figure.find(board[row][col]);
        }

        public Board set(Figure f, int row, int col) {
            validation(row,col);
            board[row][col] = f.getVal();
            return this;
        }

        private void validation(int row, int col) {
            if (row >= magicalSize || col >= magicalSize || row < 0 || col < 0) {
                throw new IllegalArgumentException("row and col have illegal vals : " +
                        "" + row + " " + col + "; board size is" + magicalSize);
            }
        }

        @Override
        public String toString() {
            StringBuilder res = new StringBuilder(prefix);
            for (int i = 0; i < magicalSize; i ++) {
                for (int j = 0; j < magicalSize; j ++) {
                   res.append(Figure.find(board[i][j]));
                }
                res.append("\n");
            }
            return res.append(prefix).toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Board board1 = (Board) o;

            return Arrays.deepEquals(board, board1.board);
        }

        @Override
        public int hashCode() {
            return Arrays.deepHashCode(board);
        }
    }

    public enum Figure {
        Q(2), K(1), _(0), ZLP(-1);
        int identifier;

        Figure(int identifier) {
            this.identifier = identifier;
        }

        public int getVal() {
            return identifier;
        }

        public static Figure find(int val) {
            for (Figure figure : values()) {
                if (figure.getVal() == val) {
                    return figure;
                }
            }
            return ZLP;
        }
    }

}
