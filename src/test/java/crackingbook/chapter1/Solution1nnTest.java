package crackingbook.chapter1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Solution1nnTest extends Solution1Test {
    private Solution1 solution = new Solution1nn();

    @Test
    public void unique() throws Exception {
        assertTrue(solution.hasAllUniqueChars("qwertyuioplkjhgfdsazxcvbnm,./';[]=-0987654321"));
        assertTrue(solution.hasAllUniqueChars("b"));
        assertTrue(solution.hasAllUniqueChars("+"));
    }

    @Test
    public void singleCharIsUnique() throws Exception {
        assertTrue(solution.hasAllUniqueChars("a"));
        assertTrue(solution.hasAllUniqueChars("+"));
    }

    @Test
    public void emptyStringIsUnique() throws Exception {
        assertTrue(solution.hasAllUniqueChars(""));
    }

    @Test
    public void nullStringIsUnique() {
        assertTrue(solution.hasAllUniqueChars(null));
    }

    @Test
    public void oneDuplicateIsNonUnique() throws Exception {
        assertFalse(solution.hasAllUniqueChars("qwertyuioplkjhgfdsazxcvbnm,./';[]=-0987654321q"));
        assertFalse(solution.hasAllUniqueChars("bb"));
    }

    @Test
    public void multipleDuplicatesIsNonUnique() throws Exception {
        assertFalse(solution.hasAllUniqueChars("qwertyuqioplkjhgfdsaazxcvbnm,./';[]q=-0987654321q"));
        assertFalse(solution.hasAllUniqueChars("bbbbb"));
        assertFalse(solution.hasAllUniqueChars("_b_"));
    }
}