package crackingbook.chapter8;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertThat;

/**
 *
 */
public class Solution4Test {

    private Solution4<Integer> sol = new Solution4<>();

    @Test
    public void allSubsOutputsCorrectly() {
        Set<Integer> init = Sets.newHashSet(1,2,3,4);
        Set<Set> expectedSublists = Sets.newHashSet(h(1),h(2), h(3), h(4),
                h(1,2), h(1,3), h(1,4), h(2,3), h(2,4), h(3,4),
                h(1,2,3), h(1,2,4), h(1,3,4), h(2,3,4), h(1,2,3,4));
        Set<Set<Integer>> actualSubsets = sol.getAllSubsets(init);
        assertThat(actualSubsets.size(), CoreMatchers.equalTo(expectedSublists.size()));
        for (Set<Integer> actualSubset : actualSubsets) {
            assertThat(expectedSublists + " \n " + actualSubset,
                    expectedSublists.contains(actualSubset), CoreMatchers.is(true));
        }
    }

    private Set<Integer> h(Integer ... ints) {
        return new HashSet<>(Arrays.asList(ints));
    }
}