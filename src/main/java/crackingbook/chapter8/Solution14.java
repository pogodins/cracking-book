package crackingbook.chapter8;

import java.util.ArrayList;
import java.util.List;

/**
 *  Given a boolean expression consisting of the symbols 0 (false), 1 (true), &
 (AND), I (OR), and /\ (XOR), and a desired boolean result value result, implement a function to
 count the number of ways of parenthesizing the expression such that it evaluates to result. The
 expression should be fully parenthesized (e.g., ( 0) A( 1)) but not extraneously (e.g., ( ( ( 0)) /\ ( 1)) ).
 EXAMPLE
 countEval('1^0|0|1', false) -> 2
 countEval('0&0&0&1^1|0', true)-> 10
 */
public class Solution14 {

    public int countEval(String eval, boolean result) {
        return -1;
    }
}

enum Eval {
    T('0'), F('1'), XOR('^'), AND('&'), OR('|');
    private final char val;

    Eval(char val) {
        this.val = val;
    }

    public char get() {
        return val;
    }
    
    public static Eval get(char eval) {
        for (Eval currEval : values()) {
            if (currEval.get() == eval) {
                return currEval;
            }
        }
        throw new IllegalArgumentException(" no such eval as " + eval);
    }
    
    public static List<Eval> toEvalList(String eval) {
        List<Eval> list = new ArrayList<>(eval.length());
        for (char cur : eval.toCharArray()) {
            list.add(get(cur));
        }
        return list;
    }
}
