package crackingbook.chapter8;

import java.util.List;

/**
 * Given a grid with n rows and m columns
 * Given a robot in top left corner
 * Robot can move only right and down
 * Certain cells in grid are "off limits" -> robot cannot step on them
 * Find way to bottom right corner
 *
 * Return empty list if there are no way to bottomRight or if grid is contains only one cell
 */
public class Solution2 {

    /**
     * @param grid true cell is available for move, false cell is unavailable for move
     * @return list that contains moving instructions, E.G. List(D,R,R,D,...)
     * <p>
     * let's say if grid looks like array
     * {{0,0,0,0},
     * {0,0,1,0},
     * {0,0,0,0}}
     * than 1 gonna be at location [1][2] which means that
     * x = gorizontal = second val
     * y = vertial = first val
     * <p>
     * so array gonna looks like grid[y][x] => just to make it easier to test
     * or you can do as grid[x][y] - just swap directions R and D
     */
    public List<Direction> findAWay(boolean[][] grid) {
        //TODO implement
        return null;
    }
}

/**
 * right, down
 */
enum Direction {
    R, D
}


