package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * implement a method to implement basic string compression using the counts of repeated characters:
 * E.G. aaaaaabccccccccddd -> a6bc8d3
 * if complessed length >= original => return original string
 * allowed only: [A-Za-z]+
 */
public class Solution6 {
    String compress(String str) {
        throw new NoImplException();
    }
}
