package crackingbook.chapter1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Root on 1/6/2017.
 */
public class Solution4Test {
    Solution4 sol = new Solution4();

    @Test
    public void positivePolindomTest() {
        assertTrue(sol.isPalindromePermutation("asd fsd a"));
        assertTrue(sol.isPalindromePermutation(" dfghbsdfjgkhv asan 1235kldafjbg dfghbsdfjgkhv asan 1235kldafjbg  191    "));
        assertTrue(sol.isPalindromePermutation("     "));
        assertTrue(sol.isPalindromePermutation("  @#$%^&*&^%$# +_+_--@uplz ][apuzal][ "));
    }

    @Test
    public void negativePolindomTest() {
        assertFalse(sol.isPalindromePermutation("asd fd a"));
        assertFalse(sol.isPalindromePermutation(" dfghbsdfjghv asan 1235kldafjbg dfghbsdfjgkhv asan 1235kldafjbg  191    "));
        assertFalse(sol.isPalindromePermutation("    ds "));
        assertFalse(sol.isPalindromePermutation("  @#$%^&*&^%# +_+_--@uplz ][apuzal][ "));
    }

    @Test
    public void xorImplementationTest() {
        //āခᄀ
        // this test works correct in case of xor implementstion; which is partially correct only
        assertFalse(sol.isPalindromePermutation("" + '\u0101' + '\u1001' + '\u1100'));
    }
}