package crackingbook.chapter8;

import java.util.Stack;

/**
 * Towers of Hanoi
 * you have 3 towers and N discs of different sizes which can slide onto any tower;
 * The pazzle starts with discs sorted in adscending order
 *
 * a disc cannot be places on a smaller disc
 */
public class Solution6 {

    private final Tower first;
    private final Tower second;
    private final Tower third;

    public Solution6(int n) {
        first = new Tower(n);
        second = new Tower();
        third = new Tower();
    }

    public Tower getAnotherFullFiledTower() {

        //TODO implement

        return third;
    }

    class Tower extends Stack<Integer> {

        private Tower () {}

        private Tower(int n) {
            // n discs in acsending order
            for (int i = n; i > 0; i++) {
                push(i);
            }
        }

        @Override
        public Integer push(Integer integer) {
            if (peek() < integer) {
                throw new IllegalStateException("unallowed to place bigger disc on smaller one");
            }
            return super.push(integer);
        }
    }

}

