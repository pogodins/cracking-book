package crackingbook.chapter1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class Solution9Test {
    Solution9 sol = new Solution9();

    @Test
    public void positiveTestWithRotatedAndEqualString() {
        String str = "hellobadass";
        String rotation = "obadasshell";

        testPattern(str, rotation, true);
        testPattern("dfgdf#%*&^#%", "f#%*&^#%dfgd", true);
        testPattern("123", "123", true);
    }

    @Test
    public void negaliveTestWithMissedLettersAndEqualityWithMissedLetters() {
        testPattern("helloTheString", "StrinhelloThe", false);
        testPattern("seven7", "seven", false);
        testPattern("withSpecialSymbiols1*&^@$", "ymbiols1*&^@$withSpecial", false);
    }

    private void testPattern(String s1, String s2, boolean rotation) {
        if (rotation) {
            assertTrue(s2 + " IS ROTATION OF " + s1 + ". But you have opposite result",
                    sol.isRotation(s1, s2));
        } else {
            assertFalse(s2 + " IS NOT ROTATION OF " + s1 + ". But you have opposite result",
                    sol.isRotation(s1, s2));
        }
    }

    @Test
    public void totalEmptyness() {
        assertFalse(sol.isRotation("", "123"));
        assertFalse(sol.isRotation("asd", ""));

        assertTrue(sol.isRotation("", ""));
    }

    @Test
    public void nullWillReturnFalseAnyway() {
        assertFalse(sol.isRotation(null, ""));
        assertFalse(sol.isRotation(null, null));
        assertFalse(sol.isRotation("", null));
    }

}