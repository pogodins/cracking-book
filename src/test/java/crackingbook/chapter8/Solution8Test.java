package crackingbook.chapter8;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertTrue;

/**
 * Created by Root on 4/15/2017.
 */
public class Solution8Test {

    @Test
    public void successfulBasicTest() {
        final String s = "ommmggm";
        Solution8 sol = new Solution8();
        Set<String> expected = Sets.newHashSet("omg", "mgo", "ogm", "mog", "gom", "gmo");

        Set<String> actual = sol.permsWithDups(s);
        for (String exp : expected) {
            assertTrue("\n wait a minute, it's not a correct data : " + actual + " \n we expected" + expected,
                    actual.contains(exp));
        }
    }

}