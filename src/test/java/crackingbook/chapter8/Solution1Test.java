package crackingbook.chapter8;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.*;

/**

 */
public class Solution1Test {

    @Test
    public void successPassShouldBeCorrect() {
        int n = 15;
        int expected = 5768;

        assertThat(Solution1.countWays(n), CoreMatchers.equalTo(expected));
    }
}