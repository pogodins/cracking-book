package crackingbook.chapter8;

/**
 * Coins: Given an infinite number of quarters (25 cents), dimes (10 cents), nickels (5 cents), and
 pennies (1 cent), write code to calculate the number of ways of representing n cents.
 */
public class Solution11 {

    public int numberOfWays(int n) {
        //TODO implement
        return -1;
    }

    /**
     * quarter, dime, nickel, cent
     */
    public enum Coin {
        Q(25), D(10), N(5), C(1);

        final int worth;
        Coin(int worth) {
            this.worth = worth;
        }

        public int get() {
            return worth;
        }
    }
}
