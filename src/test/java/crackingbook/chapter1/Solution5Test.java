package crackingbook.chapter1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Root on 1/6/2017.
 */
public class Solution5Test {

    Solution5 sol = new Solution5();

    @Test
    public void positiveTest() {
        String str         = "qwertyuiopasdfghjkl][][][][]]123456789!@#$%^&*()";
        String strAdded    = "qwertyuiopasdfghjkl][][]*[][]]123456789!@#$%^&*()";
        String strReplaced = "qwertyuiopasdfghjkl][][]*][]]123456789!@#$%^&*()";
        String strRemoved  = "qwertyuiopasdfghjkl][][]][]]123456789!@#$%^&*()";
        assertTrue(sol.oneAway("play", "pla"));
        assertTrue(sol.oneAway("play", "plays"));
        assertTrue(sol.oneAway("play", "pday"));
        assertTrue(sol.oneAway(str, strAdded));
        assertTrue(sol.oneAway(str, strRemoved));
        assertTrue(sol.oneAway(str, strReplaced));

        assertTrue(sol.oneAway(str, str));
    }

    @Test
    public void negativeTest() {
        String str = "qwertyuiopasdfghjkl][][][][]]123456789!@#$%^&*()";
        String strAdded = "qwertyuiopasdfghjkl][][][][]]12345678[]9!@#$%^&*()";
        String strReplaced = "qwertyuiopasdfghjkl][][][][]]123[]6789!@#$%^&*()";
        String strRemoved = "qwertyuiopasdfghjkl][][][][]]1234589!@#$%^&*()";
        assertFalse(sol.oneAway("play", "pl"));
        assertFalse(sol.oneAway("play", "playss"));
        assertFalse(sol.oneAway("play", "pdvy"));
        assertFalse(sol.oneAway(str, strAdded));
        assertFalse(sol.oneAway(str, strRemoved));
        assertFalse(sol.oneAway(str, strReplaced));
    }

    @Test
    public void edgeCases() {
        assertTrue(sol.oneAway("", "a"));
        assertTrue(sol.oneAway("", ""));

        assertFalse(sol.oneAway("", " a"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullStringCausesAnException() {
        sol.oneAway(null, "");
    }
}