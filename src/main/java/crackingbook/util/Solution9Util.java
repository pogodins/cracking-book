package crackingbook.util;

/**
 * isSubstring for solution9
 */
public class Solution9Util {
    private boolean used = false;

    public boolean isSubstring(String str, String sub) {
        if(used) {
            throw new IllegalStateException("use another object");
        }
        used = true;
        return str.contains(sub);
    }
}
