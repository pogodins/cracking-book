package crackingbook.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * this test should pass always
 */
public class Solution9UtilTest {
    private Solution9Util getS() {
        return new Solution9Util();
    }

    @Test
    public void isSubstringWorksCorrect() {
        final String s1 = "asdsdgsergdsfgbesrgsdfg";
        final String s2 = "sdgsergdsfgbesr";
        Assert.assertTrue(getS().isSubstring(s1, s2));
        Assert.assertTrue(getS().isSubstring(s1, s1));
        Assert.assertTrue(getS().isSubstring(s2, ""));
        Assert.assertFalse(getS().isSubstring(s2, s1));
    }

    @Test(expected = IllegalStateException.class)
    public void notAllowedMoreThanOneTime() {
        Solution9Util sol = getS();
        sol.isSubstring("","");
        sol.isSubstring("","");
    }

}
