package crackingbook.chapter8;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**

 */
public class Solution5Test {
    Solution5 sol = new Solution5();

    @Test
    public void positiveResult() {
        int first = 25;
        int second = 235;
        assertThat(sol.multiply(first, second), CoreMatchers.equalTo(first * second));
    }

}