package crackingbook.chapter2;

import crackingbook.util.LList;

/**
 * Created by Root on 1/11/2017.
 */
public class Solution4 {

    /**
     *
     * @param list
     * @param pivot
     * @return
     */
    public LList<Integer> partialSort(LList<Integer> list, int pivot) {
        LList<Integer> rightFirst = null;
        LList<Integer> rightLast = null;

        LList<Integer> iter = list;
        LList<Integer> prev = null;
        while(iter != null) {
            if (iter.val >= pivot) {
                final LList<Integer> next = iter.next;
                if (prev != null) {
                    prev.next = iter.next;
                }
                if (rightFirst == null) {
                    rightFirst = iter;
                } else {
                    rightLast.next = iter;
                }
                rightLast = iter;
                rightLast.next = null;
                iter = next;
            } else {
                prev = iter;
                iter = iter.next;
            }
        }

        if (rightFirst != null && prev != null) {
            prev.next = rightFirst;
        }
        return list;
    }

    public LList removeAndAddTo(LList element, LList previous, LList target) {
        if (previous != null) {
            previous.next = element.next;
        } else {

        }
        if (target != null) {
            target.next = element;
            return target;
        } else {
            return element;
        }
    }
}
