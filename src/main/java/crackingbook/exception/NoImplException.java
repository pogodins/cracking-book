package crackingbook.exception;

/**
 * if you have no implementation yet
 */
public class NoImplException extends RuntimeException {
    public NoImplException() {
        super("Where is your implementation, young man ?");
    }
}
