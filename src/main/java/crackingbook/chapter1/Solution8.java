package crackingbook.chapter1;

import crackingbook.exception.NoImplException;

/**
 * If an element in MxN matrix is 0 it's entire row and column are set to 0
 * E.G.
 * Input 1 2 3 11
 *       4 5 6 12
 *       7 8 9 0
 *       1 0-3 45
 *
 * Output 1 0 3 0
 *        4 0 6 0
 *        0 0 0 0
 *        0 0 0 0
 *
 */
public class Solution8 {
    public int [][] setZeros(int [][] matrix) {
        throw new NoImplException();
    }
}
