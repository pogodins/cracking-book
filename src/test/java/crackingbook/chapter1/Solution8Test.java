package crackingbook.chapter1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static crackingbook.util.ParadiseTools.mas;
import static org.junit.Assert.assertThat;

public class Solution8Test {

    Solution8 sol = new Solution8();

    @Test
    public void positiveZerosOnTopRightAndBottomLeft() {
        int [][] init =
                {{1,2,3,1,5,6,0},
                 {4,4,5,5,6,7,8},
                 {0,8,9,3,4,5,8}};

        int [][] expected =
                {{0,0,0,0,0,0,0},
                 {0,4,5,5,6,7,0},
                 {0,0,0,0,0,0,0}};

        positiveLikeTest(init, expected);
    }

    @Test
    public void positiveZeroTopAndInTheMiddle() {
        int [][] init =
                {{1,2,3,0,5,6,6},
                 {4,2,5,5,6,7,8},
                 {4,0,5,5,6,7,8},
                 {8,8,9,3,4,5,8}};

        int [][] expected =
                {{0,0,0,0,0,0,0},
                 {4,0,5,0,6,7,8},
                 {0,0,0,0,0,0,0},
                 {8,0,9,0,4,5,8}};

        positiveLikeTest(init, expected);
    }

    @Test
    public void positiveZeroLeftInTheMiddle() {
        int [][] init =
                {{1,2,3,2,5,6,6},
                 {0,2,5,5,6,7,8},
                 {4,9,5,5,6,0,8},
                 {8,8,9,4,4,5,8}};
        int [][] expected =
                {{0,2,3,2,5,0,6},
                 {0,0,0,0,0,0,0},
                 {0,0,0,0,0,0,0},
                 {0,8,9,4,4,0,8}};

        positiveLikeTest(init, expected);
    }

    @Test
    public void singleElementMatrix() {
        positiveLikeTest(new int[][]{{1}}, new int [][]{{1}});
    }

    @Test
    public void emptyMatrix() {
        positiveLikeTest(new int[][]{}, new int [][]{});
    }

    @Test
    public void weirdlyEmptyMatrix() {
        positiveLikeTest(new int[][]{{}}, new int [][]{{}});
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullMatrixCausesIllegalArgException() {
        sol.setZeros(null);
    }

    private void positiveLikeTest(int[][] init, int[][] expected) {
        int [][] actual = sol.setZeros(init);
        mas(actual);
        assertThat(" \nExpected : \n" + mas(expected) +
                        "but unexpectedly (due to ass-handed developer) was : \n" + mas(actual),
                actual, CoreMatchers.equalTo(expected));
    }

}