package crackingbook.util;

/**
 * Created by Root on 1/9/2017.
 */
public class ParadiseTools {

    /**
     * matrix as string
     * @param matrix int[n][m] array
     * @return string that will represent matrix like:
     * 1 2 3
     * 4 5 6
     * 7 8 9
     *
     * instead of [[1,2,3],[4,5,6],[7,8,9]]
     *
     * n may != m
     */
    public static String mas(int [][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                sb.append(matrix[i][j] + " ");
            }
            sb.append("\n");
        }
        return sb.append("\n").toString();
    }
}
