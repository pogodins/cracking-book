package crackingbook.chapter1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * Created by Root on 1/7/2017.
 */
public class Solution6Test {

    Solution6 sol = new Solution6();

    @Test
    public void positiveTest() {
        String s = "aaabbbbcccccdddddd";
        String compS = "a3b4c5d6";
        assertThat(sol.compress(s), equalTo(compS));
    }

    @Test
    public void positiveTestSkipCompress() {
        String s = "asdfgasdfgasdwerlknmdfl;hkbnsdfl;ghn";
        assertThat(sol.compress(s), equalTo(s));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullCauseIllegalArgException(){
        sol.compress(null);
    }

    @Test
    public void emptyStringReturnItself() {
        assertThat(sol.compress(""), equalTo(""));
    }

}