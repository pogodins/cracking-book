package crackingbook.chapter1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * Created by Root on 1/7/2017.
 */
public class Solution7Test {

    Solution7 sol = new Solution7();

    @Test
    public void positive() {
        int [][] init =
                {{1,2,3},
                 {4,5,6},
                 {7,8,9}};

        int[][] exp1 =
                {{3,6,9},
                 {2,5,8},
                 {1,4,7}};

        int[][] exp2 =
                {{7,4,1},
                 {8,5,2},
                 {9,6,3}};

        assertThat(sol.rotate(init), equalTo(true));
        assertThat(init, anyOf(equalTo(exp1), equalTo(exp2)));
    }

}