package crackingbook.chapter8;

import com.google.common.collect.Sets;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertThat;

/**
 */
public class Solution13Test {

    @Test
    public void testSolution() {
        Box box1 = new Box(1,1,1);
        Box box2 = new Box(2,2,2);
        Box box3 = new Box(3,3,3);
        Box box4 = new Box(4,1,4);
        Box box5 = new Box(2,5,5);
        Box box6 = new Box(6,6,6);

        Set<Box> boxSet = Sets.newHashSet(box1, box2, box3, box4, box5, box6);
        assertThat(new Solution13().maxHeight(boxSet), CoreMatchers.is(12));
    }

}